﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace TetradaOnlineShop.Client
{
    public class AppConfig
    {
        public static string ConnectionString { get { return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString; } }
        public static string ProductPhotoPath
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["ProductPhotoPath"];
            }
        }
    }
}