﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TetradaOnlineShop.Client.DALC;
using TetradaOnlineShop.Client.DomainModels;
using TetradaOnlineShop.Client.Models;

namespace TetradaOnlineShop.Client.Controllers
{
    public class ProductsController : Controller
    {
    
        public ActionResult Index(ProductFilterModel filterModel)
        {

            int totalRowCount = 0;
            filterModel.DisplayLength = 20;
            filterModel.PageNumber = filterModel.PageNumber == 0 ? 1 : filterModel.PageNumber;
            IList<ProductModel> products = ProductDALC.GetProducts(filterModel, out totalRowCount);
            int totalPageCount = totalRowCount % 20 == 0 ? totalRowCount / 1 : (totalRowCount / 20 + 1);


            int cartItemCount = 0;
            if (Session["Carts"] != null)
            {
                IList<CartModel> carts = (IList<CartModel>)Session["Carts"];
                cartItemCount = carts.Sum(x => x.Quantity);
            }
            CustomerInfoFillingStatus fillingStatus = CustomerInfoFillingStatus.None;

            if (Request.Cookies["CustomerInfoFillingStatus"] != null && !string.IsNullOrEmpty(Request.Cookies["CustomerInfoFillingStatus"].Value))
            {
                if (Enum.IsDefined(typeof(CustomerInfoFillingStatus), int.Parse(Request.Cookies["CustomerInfoFillingStatus"].Value.ToString())))
                {
                    fillingStatus = (CustomerInfoFillingStatus)int.Parse(Request.Cookies["CustomerInfoFillingStatus"].Value.ToString());
                    //Response.Cookies["CustomerInfoFillingStatus"].Expires = DateTime.Now.AddDays(7); 
                }
            }

            ProductListViewModel model = new ProductListViewModel()
            {
                Products = products,
                ProductCategories = ProductDALC.GetProductCategories().ToList(),
                PageNumber = filterModel.PageNumber,
                TotalPageCount = totalPageCount,
                Name = filterModel.Name,
                ProductCategory = filterModel.ProductCategory,
                CartItemCount = cartItemCount,
                CustomerInfoFillingStatus = (int)fillingStatus
            };
            return View(model);
        }
      




        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult SaveCustomerInitialInfo(OrderModel order)
        {
            string customerInfo = JsonConvert.SerializeObject(order);

            Response.Cookies["CustomerInfo"].Value = customerInfo;
            Response.Cookies["CustomerInfo"].Expires = DateTime.Now.AddDays(7);


            Response.Cookies["CustomerInfoFillingStatus"].Value = ((int)CustomerInfoFillingStatus.Filled).ToString();
            Response.Cookies["CustomerInfoFillingStatus"].Expires = DateTime.Now.AddDays(7);


            return Json(new { result = true });
        }
     
        [HttpPost]
        public JsonResult SkipCustomerInitialInfo()
        {

            string customerInfo = JsonConvert.SerializeObject(new OrderModel
            {
                CustomerName = string.Empty,
                CustomerEmail = string.Empty,
                CustomerPhone = string.Empty,
                CustomerAddress = string.Empty
            });
            Response.Cookies["CustomerInfo"].Value = customerInfo;
            Response.Cookies["CustomerInfo"].Expires = DateTime.Now.AddDays(7);

            Response.Cookies["CustomerInfoFillingStatus"].Value = ((int)CustomerInfoFillingStatus.Skipped).ToString();
            Response.Cookies["CustomerInfoFillingStatus"].Expires = DateTime.Now.AddDays(7);


            return Json(new { result = true });
        }
  
        public ActionResult Details(Guid productOid)
        {
            int cartItemCount = 0;
            if (Session["Carts"] != null)
            {
                IList<CartModel> carts = (IList<CartModel>)Session["Carts"];
                cartItemCount = carts.Sum(x => x.Quantity);
            }
            ProductDetailsViewModel model = new ProductDetailsViewModel
            {
                ProductInfo = ProductDALC.GetProductById(productOid),
                PhotoUrls = ProductDALC.GetProductPhotos(productOid).ToList(),
                CartItemCount = cartItemCount
            };
            return View(model);
        }
   
        public ActionResult CartItems()
        {
            int cartItemCount = 0;
            IList<CartModel> carts = null;
            if (Session["Carts"] != null)
            {
                carts = (IList<CartModel>)Session["Carts"];
                cartItemCount = carts.Sum(x => x.Quantity);
            }
            CartItemsViewModel viewModel = new CartItemsViewModel();
            viewModel.Products = new List<ProductModel>();
            viewModel.CartItemCount = cartItemCount;
            if (carts != null)
            {
                foreach (CartModel cart in carts)
                {
                    ProductModel productModel = ProductDALC.GetCartItemByOid(cart.ProductOid);
                    productModel.Quantity = cart.Quantity;
                    viewModel.Products.Add(productModel);
                }
            }
            viewModel.Order = new OrderModel();

            if (Request.Cookies["CustomerInfoFillingStatus"] != null && Request.Cookies["CustomerInfo"] != null)
            {
                OrderModel customerInfo = JsonConvert.DeserializeObject<OrderModel>(Request.Cookies["CustomerInfo"].Value);

                viewModel.Order = customerInfo;
            }

            viewModel.Order.DocumentDiscount = ProductDALC.GetDocumentDiscount(viewModel.TotalAmount);
            viewModel.Order.TotalGrandAmount = viewModel.TotalAmount - viewModel.TotalAmount * viewModel.Order.DocumentDiscount / 100;
            return View(viewModel);
        }
      
        [HttpPost]
        public JsonResult GetDocumentDiscount(decimal totalAmount)
        {
            decimal discount = 0;

            discount = ProductDALC.GetDocumentDiscount(totalAmount);
            return Json(new { discount = discount });
        }
 
        [HttpPost]
        public JsonResult AddToCart(Guid productOid, int quantity)
        {
            IList<CartModel> carts;
            if (Session["Carts"] == null)
            {
                carts = new List<CartModel>()
                {
                    new CartModel{
                        ProductOid=productOid,
                        Quantity=quantity
                    }
                };
                Session["Carts"] = carts;

            }
            else
            {
                carts = (IList<CartModel>)Session["Carts"];

                CartModel model = carts.FirstOrDefault(x => x.ProductOid == productOid);
                if (model != null)
                {
                    model.Quantity += quantity;
                }
                else
                {
                    carts.Add(new CartModel
                    {
                        ProductOid = productOid,
                        Quantity = quantity
                    });
                }

                Session["Carts"] = carts;
            }
            return Json(new { result = true, itemCount = carts.Sum(x => x.Quantity) });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateOrder(CartItemsViewModel model)
        {
            bool result = false;
            IList<CartModel> cartItems = new List<CartModel>();

            if (model.Products != null && model.Products.Any(x => x.Quantity > 0))
            {
                foreach (var productItem in model.Products.Where(x => x.Quantity > 0))
                {
                    cartItems.Add(new CartModel
                    {
                        ProductOid = productItem.Oid,
                        Quantity = productItem.Quantity,
                        SalePrice = productItem.SalePrice.Value,
                        Discount = productItem.Discount.Value
                    });
                }

            }

            if (cartItems.Any() && model.Order != null)
            {
                result = ProductDALC.CreateOrderDocument(cartItems, model.Order);
                if (result)
                {
                    Response.Cookies["CustomerInfo"].Value = JsonConvert.SerializeObject(model.Order);
                    Response.Cookies["CustomerInfo"].Expires = DateTime.Now.AddDays(7);
                    Session.Remove("Carts");
                }

            }

            return Json(new { result = result });

        }
    }
}