﻿
jQuery(document).ready(function ($) {

    createEventHandlers();
    $('#cart-item-count-link span').text($('#CartItemCount').val());

})

function createEventHandlers() {
    checkFilterPanelStateHandler();
    $('#product-category-filter').select2({
        placeholder: 'Seçin...',
        allowClear: true,
    }); 

    $('.panel [data-action=collapse]').click(function (e) {
        filterPanelStateChangeHandler();
    });

    //$('#filter-products').on('click', function () {
    //    filterProductsHandler();
    //});

    $('#clear-filter-fields').on('click', function () {
        clearFilterFieldsHandler();
    });
  
}

function filterProductsHandler() {
    $("#filter-products").trigger("click"); 
}

function clearFilterFieldsHandler() {
    $("#ProductSearchForm input[type='text']").val('');
    $('#product-category-filter').val(null).trigger('change');
    filterProductsHandler();

}
function checkFilterPanelStateHandler() {
    if ($.cookie('productsFilterState') === null || $.cookie('productsFilterState') === undefined) {
        $.cookie('productsFilterState', 1);
    }
}
function filterPanelStateChangeHandler() {
    var currentState = parseInt($.cookie('productsFilterState'));
    $.cookie('productsFilterState', currentState === 1 ? 0 : 1);
}

function AddToCart(productOid) {
 
    var quantity = 1;

    $.ajax({
        url: addProductToCartUrl,
        type: 'POST',
        data: {
            productOid: productOid,
            quantity: quantity
        },
        success: function (response) {
            if (response.result) {
                $.jGrowl('Sifarişə əlavə edildi', {
                    position: 'bottom-right',
                    theme: 'bg-success',
                    header: 'Məlumat'
                });

                $('#cart-item-count-link span').text(response.itemCount);
            }
            else {
                $.jGrowl('Xəta baş verdi', {
                    position: 'bottom-right',
                    theme: 'bg-danger',
                    header: 'Məlumat'
                });
            }

        }
    });
}