﻿var _customerInitialModal;

jQuery(document).ready(function ($) {

    $('.format-phone-number').formatter({
        pattern: '({{999}}) {{999}} - {{9999}}'
    });

    if ($('#CustomerInfoFillingStatus').length && $('#CustomerInfoFillingStatus').val() === "0") {
        $('#customer_initial_modal').modal('show');
    }



    _customerInitialModal = $('#customer_initial_modal');
    var _$form = _customerInitialModal.find('form');


    _$form.find('button[type="submit"]').click(function (e) {
        e.preventDefault();

        if (!_$form.valid()) {
            return;
        }
        var frmData = new FormData(_$form[0]);

        var light = _customerInitialModal;
        $(light).block({
            message: '<i class="icon-spinner spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });


        $.ajax({
            url: '/Products/SaveCustomerInitialInfo',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                _customerInitialModal.modal('hide');
                $(light).unblock();
            },
            error: function (err) {

            }
        });


    });


    $('#btn-skip-customer-initial-modal').click(function (e) {

        $.ajax({
            url: '/Products/SkipCustomerInitialInfo',
            type: "POST",
            contentType: false,
            processData: false,
            success: function (data) {
                _customerInitialModal.modal('hide');
            },
            error: function (err) {

            }
        });

    });
 

})

 