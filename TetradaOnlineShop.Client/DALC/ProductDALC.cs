﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TetradaOnlineShop.Client.DomainModels;
using TetradaOnlineShop.Client.Models;

namespace TetradaOnlineShop.Client.DALC
{
    public class ProductDALC
    {
        public static IList<ProductModel> GetProducts(ProductFilterModel filterModel, out int totalRowCount)
        {
            IList<ProductModel> products = new List<ProductModel>();
            using (SqlConnection con = new SqlConnection(AppConfig.ConnectionString))
            {

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select " +
                                        "       p.Oid," +
                                        "       p.Name ProductName," +
                                        "	   pc.Name ProductCategoryName," +
                                        "	   u.Name UnitName," +
                                        "	   p.SalePrice," +
                                        "	   p.Discount," +
                                        "	   p.CampaignExists," +
                                        "	   p.ExistsInStore," +
                                        "	   p.Description," +
                                        "	   ISNULL(fsp.FileName,'') FileName," +
                                        "	   fsp.FileOid" +
                                        " from Product p" +
                                        " left join ProductCategory pc on pc.Oid=p.ProductCategory" +
                                        " left join Unit u on p.Unit=p.Unit" +
                                        " outer apply (select TOP 1 fsp.FileName,fsp.Oid FileOid " +
                                        " from ProductPhoto pp " +
                                        " left  join FileSystemStoreObject fsp on fsp.Oid = pp.Photo " +
                                        " where pp.GCRecord is null and pp.Product = p.Oid order by pp.OrderNumber) fsp" +
                                        " where p.GCRecord is null and p.ExistsInStore=1 and " +
                                        " LOWER(p.Name) like @ProductName " +
                                        " and (@ProductCategory is null or p.ProductCategory=@ProductCategory)" +
                                        " order by p.Name" +
                                        " OFFSET @PageSize * (@PageNumber - 1) ROWS" +
                                        " FETCH NEXT @PageSize ROWS ONLY;  " +
                                        " select count(*) TotalRowCount from Product p2 where p2.GCRecord is null " +
                                        " and p2.ExistsInStore=1 and LOWER(p2.Name) like @ProductName" +
                                        " and ( @ProductCategory is null or p2.ProductCategory=@ProductCategory); ";
                    cmd.Parameters.AddWithValue("@ProductName", $"%{filterModel.Name?.ToLower() ?? string.Empty}%");
                    cmd.Parameters.AddWithValue("@ProductCategory", (object)filterModel?.ProductCategory ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@PageSize", filterModel.DisplayLength);
                    cmd.Parameters.AddWithValue("@PageNumber", filterModel.PageNumber);
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            products.Add(new ProductModel
                            {
                                Oid = Guid.Parse(reader["Oid"].ToString()),
                                ProductName = reader["ProductName"].ToString(),
                                ProductCategoryName = reader["ProductCategoryName"].ToString(),
                                UnitName = reader["UnitName"].ToString(),
                                SalePrice = decimal.Parse(reader["SalePrice"].ToString()),
                                Discount = decimal.Parse(reader["Discount"].ToString()),
                                CompaignExists = (bool)reader["CampaignExists"],
                                ExistInStore = (bool)reader["ExistsInStore"],
                                Decription = reader["Description"].ToString(),
                                PhotoName = string.IsNullOrEmpty(reader["FileName"].ToString()) ? string.Empty : ($"{AppConfig.ProductPhotoPath}{Guid.Parse(reader["FileOid"].ToString())}-{reader["FileName"].ToString()}")
                            });

                        }
                        reader.NextResult();

                        reader.Read();
                        totalRowCount = int.Parse(reader["TotalRowCount"].ToString());
                    }

                }

            }
            return products;
        }

        public static ProductModel GetCartItemByOid(Guid productOid)
        {
            ProductModel product = null;
            using (SqlConnection con = new SqlConnection(AppConfig.ConnectionString))
            {

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select " +
                                        "       p.Oid," +
                                        "       p.Name ProductName," +
                                        "	   pc.Name ProductCategoryName," +
                                        "	   u.Name UnitName," +
                                        "	   p.SalePrice," +
                                        "	   p.Discount," +
                                        "	   p.CampaignExists," +
                                        "	   p.ExistsInStore," +
                                        "	   p.Description," +
                                        "	   ISNULL(fsp.FileName,'') FileName," +
                                        "	   fsp.FileOid" +
                                        " from Product p" +
                                        " left join ProductCategory pc on pc.Oid=p.ProductCategory" +
                                        " left join Unit u on p.Unit=p.Unit" +
                                        " outer apply (select TOP 1 fsp.FileName,fsp.Oid FileOid " +
                                        " from ProductPhoto pp " +
                                        " left  join FileSystemStoreObject fsp on fsp.Oid = pp.Photo " +
                                        " where pp.GCRecord is null and pp.Product = p.Oid order by pp.OrderNumber) fsp" +
                                        " where p.Oid=@ProductOid";

                    cmd.Parameters.AddWithValue("@ProductOid", productOid);
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        product = new ProductModel
                        {
                            Oid = Guid.Parse(reader["Oid"].ToString()),
                            ProductName = reader["ProductName"].ToString(),
                            ProductCategoryName = reader["ProductCategoryName"].ToString(),
                            UnitName = reader["UnitName"].ToString(),
                            SalePrice = decimal.Parse(reader["SalePrice"].ToString()),
                            Discount = decimal.Parse(reader["Discount"].ToString()),
                            CompaignExists = (bool)reader["CampaignExists"],
                            ExistInStore = (bool)reader["ExistsInStore"],
                            Decription = reader["Description"].ToString(),
                            PhotoName = string.IsNullOrEmpty(reader["FileName"].ToString()) ? string.Empty : ($"{AppConfig.ProductPhotoPath}{Guid.Parse(reader["FileOid"].ToString())}-{reader["FileName"].ToString()}")
                        };

                    }

                }

            }
            return product;
        }


        public static ProductModel GetProductById(Guid productOid)
        {
            ProductModel selectedProduct = new ProductModel();
            using (SqlConnection con = new SqlConnection(AppConfig.ConnectionString))
            {

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select " +
                                        "      p.Oid," +
                                        "      p.Name ProductName," +
                                        "	   pc.Name ProductCategoryName," +
                                        "	   u.Name UnitName," +
                                        "	   p.SalePrice," +
                                        "	   p.Discount," +
                                        "	   p.CampaignExists," +
                                        "	   p.ExistsInStore," +
                                        "	   p.Description " +
                                        " from Product p" +
                                        " left join ProductCategory pc on pc.Oid=p.ProductCategory" +
                                        " left join Unit u on p.Unit=p.Unit" +
                                        " where p.Oid=@ProductOid";

                    cmd.Parameters.AddWithValue("@ProductOid", productOid);
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();


                        selectedProduct = new ProductModel
                        {
                            Oid = Guid.Parse(reader["Oid"].ToString()),
                            ProductName = reader["ProductName"].ToString(),
                            ProductCategoryName = reader["ProductCategoryName"].ToString(),
                            UnitName = reader["UnitName"].ToString(),
                            SalePrice = decimal.Parse(reader["SalePrice"].ToString()),
                            Discount = decimal.Parse(reader["Discount"].ToString()),
                            CompaignExists = (bool)reader["CampaignExists"],
                            ExistInStore = (bool)reader["ExistsInStore"],
                            Decription = reader["Description"].ToString(),
                        };



                    }

                }
            }
            return selectedProduct;

        }

        public static IList<string> GetProductPhotos(Guid productOid)
        {
            IList<string> productPhotos = new List<string>();
            using (SqlConnection con = new SqlConnection(AppConfig.ConnectionString))
            {

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT fsp.FileName," +
                                        "     fsp.Oid FileOid" +
                                        " FROM ProductPhoto pp" +
                                        " LEFT  JOIN FileSystemStoreObject fsp ON fsp.Oid = pp.Photo" +
                                        " WHERE pp.GCRecord IS NULL" +
                                        " AND pp.Product = @ProductOid" +
                                        " ORDER BY pp.OrderNumber";
                    cmd.Parameters.AddWithValue("@ProductOid", productOid);
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            productPhotos.Add($"{AppConfig.ProductPhotoPath}{Guid.Parse(reader["FileOid"].ToString())}-{reader["FileName"].ToString()}");

                        }
                    }

                }

            }
            return productPhotos;

        }
        public static IList<ProductCategoryModel> GetProductCategories()
        {
            IList<ProductCategoryModel> productCategories = new List<ProductCategoryModel>();
            using (SqlConnection con = new SqlConnection(AppConfig.ConnectionString))
            {

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select Oid,Name from ProductCategory where GCRecord is null";
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            productCategories.Add(new ProductCategoryModel
                            {
                                Oid = Guid.Parse(reader["Oid"].ToString()),
                                Name = reader["Name"].ToString()
                            });

                        }
                    }

                }

            }
            return productCategories;

        }

        public static decimal GetDocumentDiscount(decimal totalAmount)
        {
            decimal discount = 0;
            using (SqlConnection con = new SqlConnection(AppConfig.ConnectionString))
            {

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "  select TOP 1 Discount from DiscountDocument" +
                                    "   where  GCRecord is null and SYSDATETIME() between StartDate and EndDate " +
                                    "   and DiscountSchemaAmount<=@TotalAmount" +
                                    "   order by DiscountSchemaAmount desc ";


                    cmd.Parameters.AddWithValue("@TotalAmount", totalAmount);
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {

                            discount = decimal.Parse(reader["Discount"].ToString());
                        }
                    }

                }
            }
            return discount;

        }


        public static bool CreateOrderDocument(IList<CartModel> cartItems, OrderModel order)
        {
            bool orderNumberFound = false;
            using (SqlConnection con = new SqlConnection(AppConfig.ConnectionString))
            {

                SqlTransaction transaction = null;
                con.Open();
                transaction = con.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

                try
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Transaction = transaction;

                        #region GetOrderNumber

                        cmd.CommandText = "Select Oid from IDGeneratorTable where   Type='TetradaOnlineShop.Module.BusinessObjects.documents.OrderDocument'";
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                order.OrderNumber = int.Parse(reader["Oid"].ToString()) + 1;
                                orderNumberFound = true;
                            }
                            else
                            {
                                order.OrderNumber = 1;
                            }
                        }
                        if (!orderNumberFound)
                        {
                            cmd.CommandText = "INSERT INTO [dbo].[IDGeneratorTable]" +
                                                "           ([ID]" +
                                                "           ,[Type]" +
                                                "           ,[Prefix]" +
                                                "           ,[Oid]" +
                                                "           ,[OptimisticLockField])" +
                                                "     VALUES" +
                                                "           (NEWID()" +
                                                "           ,'TetradaOnlineShop.Module.BusinessObjects.documents.OrderDocument'" +
                                                "           ,''" +
                                                "           ,1" +
                                                "           ,0)";

                        }
                        else
                        {
                            cmd.CommandText = "update IDGeneratorTable  set Oid=@OrderNumber where  Type='TetradaOnlineShop.Module.BusinessObjects.documents.OrderDocument'";
                            cmd.Parameters.AddWithValue("@OrderNumber", order.OrderNumber.Value);
                        }
                        cmd.ExecuteNonQuery();
                        #endregion


                        cmd.Parameters.Clear();
                        order.Oid = Guid.NewGuid();
                        cmd.CommandText = "INSERT INTO [dbo].[OrderDocument]" +
                                            "           ([Oid]" +
                                            "           ,[TotalGrandAmount]" +
                                            "           ,[OrderDate]" +
                                            "           ,[OrderNumber]" +
                                            "           ,[CustomerName]" +
                                            "           ,[CustomerPhone]" +
                                            "           ,[CustomerEmail]" +
                                            "           ,[CustomerAddress]" +
                                            "           ,[OrderCount]" +
                                            "           ,[OrderStatus]" +
                                            "           ,[DeliveryStatus]" +
                                            "           ,[Printed]" +
                                            "           ,[Note]" + 
                                            "           ,[DocumentDiscount]" +
                                            "           ,[OptimisticLockField]" +
                                            "           ,[GCRecord])" +
                                            "     VALUES" +
                                            "           (@Oid" +
                                            "           ,@TotalGrandAmount" +
                                            "           ,SYSDATETIME()" +
                                            "           ,@OrderNumber" +
                                            "           ,@CustomerName" +
                                            "           ,@CustomerPhone" +
                                            "           ,@CustomerEmail" +
                                            "           ,@CustomerAddress" +
                                            "           ,@OrderCount" +
                                            "           ,@OrderStatus" +
                                            "           ,@DeliveryStatus" +
                                            "           ,@Printed" +
                                            "           ,@Note" + 
                                            "           ,@DocumentDiscount" +
                                            "           ,0" +
                                            "           ,NULL)";
                        cmd.Parameters.AddWithValue("@Oid", order.Oid.Value);
                        cmd.Parameters.AddWithValue("@TotalGrandAmount", order.TotalGrandAmount);
                        cmd.Parameters.AddWithValue("@OrderNumber", $"T-{order.OrderNumber.Value.ToString("0000000")}");
                        cmd.Parameters.AddWithValue("@CustomerName", order.CustomerName);
                        cmd.Parameters.AddWithValue("@CustomerPhone", order.CustomerPhone);
                        cmd.Parameters.AddWithValue("@CustomerEmail", order.CustomerEmail??string.Empty);
                        cmd.Parameters.AddWithValue("@CustomerAddress", order.CustomerAddress);
                        cmd.Parameters.AddWithValue("@OrderCount", cartItems.Sum(x => x.Quantity));
                        cmd.Parameters.AddWithValue("@OrderStatus",1);
                        cmd.Parameters.AddWithValue("@DeliveryStatus", 1); 
                        cmd.Parameters.AddWithValue("@Printed", 0);
                        cmd.Parameters.AddWithValue("@Note", order.Note ?? string.Empty);

                        cmd.Parameters.AddWithValue("@DocumentDiscount", order.DocumentDiscount);
                        cmd.ExecuteNonQuery();


                        foreach (var item in cartItems)
                        {
                            cmd.Parameters.Clear();

                            cmd.CommandText = "INSERT INTO [dbo].[OrderDocumentDetail]" +
                                                "           ([Oid]" +
                                                "           ,[Product]" +
                                                "           ,[Count]" +
                                                "           ,[SalePrice]" +
                                                "           ,[PurchasePrice]" +
                                                "           ,[Discount]" +
                                                "           ,[OrderDocument]" +
                                                "           ,[OptimisticLockField]" +
                                                "           ,[GCRecord])" +
                                                "     VALUES" +
                                                "           (NEWID()" +
                                                "           ,@ProductOid" +
                                                "           ,@Quantity" +
                                                "           ,@SalePrice" +
                                                "           ,(select PurchasePrice from Product where Oid=@ProductOid)" +
                                                "           ,@Discount" +
                                                "           ,@OrderDocumentOid" +
                                                "           ,0" +
                                                "           ,NULL)";
                            cmd.Parameters.AddWithValue("@ProductOid", item.ProductOid);
                            cmd.Parameters.AddWithValue("@Quantity", item.Quantity);
                            cmd.Parameters.AddWithValue("@SalePrice", item.SalePrice);
                            cmd.Parameters.AddWithValue("@Discount", item.Discount);
                            cmd.Parameters.AddWithValue("@OrderDocumentOid", order.Oid);
                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();


                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    // logger.Error(ex, "Error occured in ProcurementDALC.CreateProcurement method");
                    return false;
                }
                finally
                {
                    con.Close();
                }

            }

            return true;
        }


    }
}