﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetradaOnlineShop.Client.DomainModels
{
    public class ProductCategoryModel
    {
        public Guid Oid { get; set; }

        public string Name { get; set; }

    }
}