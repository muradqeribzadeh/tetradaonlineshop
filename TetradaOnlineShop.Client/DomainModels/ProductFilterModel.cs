﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TetradaOnlineShop.Client.Models;

namespace TetradaOnlineShop.Client.DomainModels
{
    public class ProductFilterModel
    {
        public string Name { get; set; }
        public Guid? ProductCategory { get; set; }
        public int PageNumber { get; set; }
        public int DisplayLength { get; set; }
    }
}