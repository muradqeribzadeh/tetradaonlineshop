﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetradaOnlineShop.Client.DomainModels
{
    public class ProductModel
    {
        public Guid Oid { get; set; }
        public string ProductName { get; set; }
        public string ProductCategoryName { get; set; }
        public string UnitName { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? Discount { get; set; }
        public bool? CompaignExists { get; set; }
        public bool? ExistInStore { get; set; }
        public string PhotoName { get; set; }
        public string Decription { get; set; }

        public int Quantity { get; set; }
        public decimal TotalCost
        {
            get
            {
                if (SalePrice.HasValue && Discount.HasValue)
                {
                    return (SalePrice.Value - SalePrice.Value * Discount.Value / 100);
                }
                else
                {
                    return 0;
                }
            }
        }
        public decimal Amount
        {
            get
            {
                if (SalePrice.HasValue && Discount.HasValue)
                {
                    return (SalePrice.Value - SalePrice.Value * Discount.Value / 100) * Quantity;

                }
                else
                {
                    return 0;
                }
            }
        }
    }
}