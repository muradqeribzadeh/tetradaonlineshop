﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetradaOnlineShop.Client.Models
{
    public class CartModel
    {
        public Guid ProductOid { get; set; }
        public int Quantity { get; set; }
        public decimal SalePrice { get; set; }
        public decimal Discount { get; set; }
    }
}