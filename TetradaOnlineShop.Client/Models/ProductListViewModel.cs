﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TetradaOnlineShop.Client.DomainModels;

namespace TetradaOnlineShop.Client.Models
{
    public class ProductListViewModel
    {
        public IList<ProductModel>  Products { get; set; }
        public IReadOnlyList<ProductCategoryModel> ProductCategories { get; set; }
        public string Name { get; set; }
        public Guid? ProductCategory { get; set; }
        public int PageNumber { get; set; }
        public int TotalPageCount { get; set; }
        public int CartItemCount { get; set; }
        public int CustomerInfoFillingStatus { get; set; }
    }
}