﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TetradaOnlineShop.Client.DomainModels;

namespace TetradaOnlineShop.Client.Models
{
    public class CartItemsViewModel
    {
        public IList<ProductModel> Products { get; set; }
        public OrderModel Order { get; set; }
        public decimal TotalAmount
        {

            get
            {
                if (Products != null)
                {
                    return Products.Sum(x => x.Amount);
                }
                else
                {
                    return 0;
                }
            }
        }
        public int CartItemCount { get; set; }
    }
}