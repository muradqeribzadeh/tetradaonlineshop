﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TetradaOnlineShop.Client.DomainModels;

namespace TetradaOnlineShop.Client.Models
{
    public class ProductDetailsViewModel
    {
        public ProductModel ProductInfo { get; set; }
        public List<string> PhotoUrls { get; set; }
        public int CartItemCount { get; set; }

    }
}