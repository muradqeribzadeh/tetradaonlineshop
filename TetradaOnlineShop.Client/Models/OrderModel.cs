﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TetradaOnlineShop.Client.Resources;

namespace TetradaOnlineShop.Client.Models
{
    public class OrderModel
    {
        public Guid? Oid { get; set; }
        public int? OrderNumber { get; set; }

        [Required(ErrorMessageResourceName = "CustomerNameRequiredMsg", ErrorMessageResourceType = typeof(OnlineShop))]
        public string CustomerName { get; set; }
        [Required(ErrorMessageResourceName = "CustomerPhoneRequiredMsg", ErrorMessageResourceType = typeof(OnlineShop))]
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        [Required(ErrorMessageResourceName = "CustomerAddressRequiredMsg", ErrorMessageResourceType = typeof(OnlineShop))]
        public string CustomerAddress { get; set; }
        public decimal DocumentDiscount { get; set; }
        public decimal TotalGrandAmount { get; set; }
        public string Note { get; set; }

    }
}