﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetradaOnlineShop.Client.Models
{
    public enum CustomerInfoFillingStatus
    {
        None=0,
        Filled=1,
        Skipped=2
    }
}