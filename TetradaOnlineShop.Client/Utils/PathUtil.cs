﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace TetradaOnlineShop.Client.Utils
{
    public static class PathUtil
    {
        public static string Convert(string path)
        {
            return ATLTech.Library.Util.CurrentProject.ConvertFilePath(path);
        }

        public static string ConvertJsOrCss(string path)
        {
            string pluginName = String.Empty;
            if (HttpContext.Current.Request.RequestContext.RouteData.Values["plugin"] != null)
            {
                pluginName = String.Format("Plugins/{0}/", HttpContext.Current.Request.RequestContext.RouteData.Values["plugin"].ToString());
            }
            string versionPath = String.Format("~/{0}{1}", pluginName, path.Replace("~/", string.Empty));
            string version = GetVersion(versionPath);
            return ATLTech.Library.Util.CurrentProject.ConvertFilePath(path) + version;
        }
        private static string GetVersion(string path)
        {
            string physicalPath = HostingEnvironment.MapPath(path);
            var version = "?v=" +
              new System.IO.FileInfo(physicalPath).LastWriteTime
                .ToString("yyyyMMddHHmmss");
            return version;
        }

    }
}