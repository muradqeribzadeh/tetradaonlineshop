﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using TetradaOnlineShop.Module.BusinessObjects.Utils;

namespace TetradaOnlineShop.Module.BusinessObjects.documents
{
    [DefaultClassOptions]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class OrderDocument : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public OrderDocument(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            OrderStatus = OrderStatus.New;
            DeliveryStatus = DeliveryStatus.InStore;

            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        protected override void OnLoaded()
        {
            fTotalGrandAmount = null; 

            base.OnLoaded();
        }

        string note;
        bool printed;
        DeliveryStatus deliveryStatus;
        OrderStatus orderStatus;
        DateTime orderDate;
        public DateTime OrderDate
        {
            get => orderDate;
            set => SetPropertyValue(nameof(OrderDate), ref orderDate, value);
        }
        string orderNumber;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string OrderNumber
        {
            get => orderNumber;
            set => SetPropertyValue(nameof(OrderNumber), ref orderNumber, value);
        }

        string barcode;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Barcode
        {
            get => barcode;
            set => SetPropertyValue(nameof(Barcode), ref barcode, value);
        }

        string customerName;
        public string CustomerName
        {
            get => customerName;
            set => SetPropertyValue(nameof(CustomerName), ref customerName, value);
        }
        string customerPhone;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerPhone
        {
            get => customerPhone;
            set => SetPropertyValue(nameof(CustomerPhone), ref customerPhone, value);
        }

        string customerEmail;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerEmail
        {
            get => customerEmail;
            set => SetPropertyValue(nameof(CustomerEmail), ref customerEmail, value);
        }

        string customerAddress;
        public string CustomerAddress
        {
            get => customerAddress;
            set => SetPropertyValue(nameof(CustomerAddress), ref customerAddress, value);
        }
        int orderCount;
        public int OrderCount
        {
            get => orderCount;
            set => SetPropertyValue(nameof(OrderCount), ref orderCount, value);
        }

        decimal documentDiscount;
        public decimal DocumentDiscount
        {
            get => documentDiscount;
            set => SetPropertyValue(nameof(DocumentDiscount), ref documentDiscount, value);
        }

        public OrderStatus OrderStatus
        {
            get => orderStatus;
            set => SetPropertyValue(nameof(OrderStatus), ref orderStatus, value);
        }

        public DeliveryStatus DeliveryStatus
        {
            get => deliveryStatus;
            set => SetPropertyValue(nameof(DeliveryStatus), ref deliveryStatus, value);
        }


        public bool Printed
        {
            get => printed;
            set => SetPropertyValue(nameof(Printed), ref printed, value);
        }


        [Persistent("TotalGrandAmount")]
        private decimal? fTotalGrandAmount = null;
        [PersistentAlias("fTotalGrandAmount")]
        public decimal? TotalGrandAmount
        {
            get
            {
                if (!IsLoading && !IsSaving && fTotalGrandAmount == null)
                    UpdateTotalGrandAmount(false);
                return fTotalGrandAmount;
            }

        }

        
         
        public string Note
        {
            get => note;
            set => SetPropertyValue(nameof(Note), ref note, value);
        }
        public void UpdateTotalGrandAmount(bool forceChangeEvents)
        {
            decimal? oldOrdersTotal = fTotalGrandAmount;
            decimal tempTotal = OrderDocumentDetails.Sum(detail => detail.GrandAmount);

            if (forceChangeEvents)
                OnChanged("TotalGrandAmount", oldOrdersTotal, fTotalGrandAmount);
        }

        [Association("OrderDocument-OrderDocumentDetails"), DevExpress.Xpo.Aggregated]
        public XPCollection<OrderDocumentDetail> OrderDocumentDetails
        {
            get
            {
                return GetCollection<OrderDocumentDetail>(nameof(OrderDocumentDetails));
            }
        }
    }
}