﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using TetradaOnlineShop.Module.BusinessObjects.sale.parameters;

namespace TetradaOnlineShop.Module.BusinessObjects.documents
{
    [DefaultClassOptions]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class OrderDocumentDetail : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public OrderDocumentDetail(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        decimal purchasePrice;
        Product product;
        public Product Product
        {
            get => product;
            set
            {
                SetPropertyValue(nameof(Product), ref product, value);

                if (!IsLoading && !IsSaving && OrderDocument != null)
                {
                    OrderDocument.UpdateTotalGrandAmount(true);
                }
            }
        }

        int count;
        public int Count
        {
            get => count;
            set
            {
                SetPropertyValue(nameof(Count), ref count, value);
                if (!IsLoading && !IsSaving && OrderDocument != null)
                {
                    OrderDocument.UpdateTotalGrandAmount(true);
                }
            }
        }

        decimal salePrice;
        public decimal SalePrice
        {
            get => salePrice;
            set
            {
                SetPropertyValue(nameof(SalePrice), ref salePrice, value);
                if (!IsLoading && !IsSaving && OrderDocument != null)
                {
                    OrderDocument.UpdateTotalGrandAmount(true);
                }
            }
        }


        public decimal PurchasePrice
        {
            get => purchasePrice;
            set => SetPropertyValue(nameof(PurchasePrice), ref purchasePrice, value);
        }


        public decimal Income
        {
            get
            {

                return SalePrice - PurchasePrice;
            }
        }
        decimal discount;
        public decimal Discount
        {
            get => discount;
            set
            {
                SetPropertyValue(nameof(Discount), ref discount, value);
                if (!IsLoading && !IsSaving && OrderDocument != null)
                {
                    OrderDocument.UpdateTotalGrandAmount(true);
                }
            }
        }



        public decimal TotalCost
        {
            get
            {
                return SalePrice * Discount / 100;
            }
        }
        public decimal Amount
        {
            get
            {
                return SalePrice * Count;
            }

        }

        public decimal GrandAmount
        {
            get
            {

                return TotalCost * Count;
            }
        }
        OrderDocument orderDocument;
        [Association("OrderDocument-OrderDocumentDetails")]
        public OrderDocument OrderDocument
        {
            get => orderDocument;
            set
            {
                OrderDocument oldDoc = orderDocument;
                SetPropertyValue(nameof(OrderDocument), ref orderDocument, value);

                if (!IsSaving && !IsLoading && oldDoc != orderDocument)
                {
                    oldDoc = oldDoc ?? orderDocument;
                    oldDoc.UpdateTotalGrandAmount(true);
                }
            }
        }




    }
}