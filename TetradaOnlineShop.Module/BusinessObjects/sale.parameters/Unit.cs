﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace TetradaOnlineShop.Module.BusinessObjects.sale.parameters
{
    [DefaultClassOptions]
    public class Unit : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Unit(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
       
        private string _name;
        private string _shortName;
        private string _internationalName;
      
       
        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                SetPropertyValue("Name", ref _name, value);
            }
        }
        public string ShortName
        {
            get
            {
                return _shortName;
            }
            set
            {
                SetPropertyValue("ShortName", ref _shortName, value);
            }
        }

        public string InternationalName
        {
            get
            {
                return _internationalName;
            }
            set
            {
                SetPropertyValue("InternationalName", ref _internationalName, value);
            }
        }
    }
}