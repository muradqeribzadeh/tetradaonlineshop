﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace TetradaOnlineShop.Module.BusinessObjects.sale.parameters
{
    [DefaultClassOptions]
    public class Product : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Product(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        string name;
        public string Name
        {
            get => name;
            set => SetPropertyValue(nameof(Name), ref name, value);
        }

        ProductCategory productCategory;
        public ProductCategory ProductCategory
        {
            get => productCategory;
            set => SetPropertyValue(nameof(ProductCategory), ref productCategory, value);
        }

        Unit unit;
        public Unit Unit
        {
            get => unit;
            set => SetPropertyValue(nameof(Unit), ref unit, value);
        }



        decimal salePrice;
        public decimal SalePrice
        {
            get => salePrice;
            set => SetPropertyValue(nameof(SalePrice), ref salePrice, value);
        }
        decimal discount;
        public decimal Discount
        {
            get => discount;
            set => SetPropertyValue(nameof(Discount), ref discount, value);
        }

        public decimal DocumentDiscount
        {
            get
            {
                return SalePrice * Discount / 100;
            }

        }
        decimal purchasePrice;
        public decimal PurchasePrice
        {
            get => purchasePrice;
            set => SetPropertyValue(nameof(PurchasePrice), ref purchasePrice, value);
        }


        bool campaignExists;
        public bool CampaignExists
        {
            get => campaignExists;
            set => SetPropertyValue(nameof(CampaignExists), ref campaignExists, value);
        }

        bool existsInStore;
        public bool ExistsInStore
        {
            get => existsInStore;
            set => SetPropertyValue(nameof(ExistsInStore), ref existsInStore, value);
        }


        string description;
        [Size(4000)]
        public string Description
        {
            get => description;
            set => SetPropertyValue(nameof(Description), ref description, value);
        }


        [Association("Product-ProductPhotos"), DevExpress.Xpo.Aggregated]
        public XPCollection<ProductPhoto> ProductPhotos
        {
            get
            {
                return GetCollection<ProductPhoto>(nameof(ProductPhotos));
            }
        }
    }
}