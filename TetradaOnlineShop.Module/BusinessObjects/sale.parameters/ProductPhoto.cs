﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FileSystemData.BusinessObjects;

namespace TetradaOnlineShop.Module.BusinessObjects.sale.parameters
{
    [DefaultClassOptions]
    [FileAttachmentAttribute("Photo")]
    public class ProductPhoto : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public ProductPhoto(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        int orderNumber;
        public int OrderNumber
        {
            get => orderNumber;
            set => SetPropertyValue(nameof(OrderNumber), ref orderNumber, value);
        }
        FileSystemStoreObject photo;
        [DevExpress.Xpo.Aggregated,NoForeignKey, ExpandObjectMembers(ExpandObjectMembers.Never)]
        [ImmediatePostData]
        public FileSystemStoreObject Photo
        {
            get => photo;
            set => SetPropertyValue(nameof(Photo), ref photo, value);
        }

        Product product;
        [Association("Product-ProductPhotos")]
        public Product Product
        {
            get => product;
            set => SetPropertyValue(nameof(Product), ref product, value);
        }
    }
}