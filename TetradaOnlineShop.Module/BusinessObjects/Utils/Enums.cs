﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TetradaOnlineShop.Module.BusinessObjects.Utils
{
    public enum OrderStatus
    {
        New = 1,
        Confirmed = 2,
        Pending = 3,
        Canceled = 4
    }

    public enum DeliveryStatus
    {
        InStore = 1,
        Send = 2,
        Delivered = 3,
        Rejected = 4
    }
}
