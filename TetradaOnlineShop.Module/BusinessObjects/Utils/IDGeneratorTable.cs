﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace TetradaOnlineShop.Module.BusinessObjects.Utils
{
    [DefaultClassOptions]
    public class IDGeneratorTable : XPBaseObject
    {
        private int _Oid;
        private Guid _ID;
        private string _Prefix;
        private string _Type;
        [Key(true)]
        public Guid ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                base.SetPropertyValue<Guid>("ID", ref this._ID, value);
            }
        }
        public string Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                base.SetPropertyValue<string>("Type", ref this._Type, value);
            }
        }
        public string Prefix
        {
            get
            {
                return this._Prefix;
            }
            set
            {
                base.SetPropertyValue<string>("Prefix", ref this._Prefix, value);
            }
        }
        public int Oid
        {
            get
            {
                return this._Oid;
            }
            set
            {
                base.SetPropertyValue("Oid", ref this._Oid, value);
            }
        }
        public IDGeneratorTable(Session session) : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}